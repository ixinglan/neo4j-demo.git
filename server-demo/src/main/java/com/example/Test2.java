package com.example;

import org.neo4j.driver.*;

import static org.neo4j.driver.Values.parameters;

/**
 * @author zhaojianqiang
 */
public class Test2 {

    public static void main(String[] args) {
        //7687是 bolt协议通信端口, 此处写你的ip
        Driver driver = GraphDatabase.driver("bolt://10.211.55.14:7687",
            AuthTokens.basic("neo4j", "123456"));
        // 获取会话对象
        Session session = driver.session();
        // shortestPath 是取最短路径
        String cql = "MATCH p=shortestPath((person:Person{name:$startName})-[*1..4]-(person2:Person {name:$endName})) return p";
        Result result = session.run(cql, parameters("startName", "长公主", "endName", "九品射手燕小乙"));
        while (result.hasNext()) {
            Record record = result.next();
            System.out.println(record);
        }
        session.close();
        driver.close();
    }
}
