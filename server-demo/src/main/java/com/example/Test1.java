package com.example;

import org.neo4j.driver.*;

import static org.neo4j.driver.Values.parameters;

/**
 * @author zhaojianqiang
 */
public class Test1 {

    public static void main(String[] args) {
        //7687是 bolt协议通信端口, 此处写你的ip
        Driver driver = GraphDatabase.driver("bolt://10.211.55.14:7687",
            AuthTokens.basic("neo4j", "123456"));
        // 获取会话对象
        Session session = driver.session();
        String cql = "MATCH(p:Person) WHERE p.money > $money return p.name AS name,p.money AS money " +
            " order by p.money";
        Result result = session.run(cql, parameters("money", 100));
        while (result.hasNext()) {
            Record record = result.next();
            System.out.println(record.get("name").asString() + "," + record.get("money").asDouble());
        }
        session.close();
        driver.close();
    }
}
