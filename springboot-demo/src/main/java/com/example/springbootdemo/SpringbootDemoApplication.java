package com.example.springbootdemo;

import com.example.springbootdemo.bean.Person;
import com.example.springbootdemo.service.Neo4jPersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@SpringBootApplication
public class SpringbootDemoApplication {

    //测试数据自已造就行
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(SpringbootDemoApplication.class, args);
        Neo4jPersonService personService = applicationContext.getBean("personService", Neo4jPersonService.class);
        //添加
        Person person = new Person();
        person.setName("testboot");
        person.setMoney(12345.45);
        person.setCharacter("A");
        person.setAge(11);
        // Person p1 = personService.save(person);
        // System.out.println(p1);
        System.out.println("--------------------------------------------------------");
        //列表
        System.out.println(personService.getAll());
        System.out.println("--------------------------------------------------------");
        //列表
        List<Person> personList = personService.personList(1000);
        System.out.println(personList);
        System.out.println("--------------------------------------------------------");
        //最短路径
        List<Person> personList2 = personService.shortestPath("长公主", "九品射手燕小乙");
        System.out.println(personList2);
        System.out.println("--------------------------------------------------------");
        //深度
        List<Person> personList3 = personService.personListDept("刘备");
        for (Person pe : personList3) {
            System.out.println(pe);
        }
    }

}
